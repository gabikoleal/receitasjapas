---
title: "Sobre"
date: 2022-10-07T23:51:23-03:00
draft: false
layout: about
---

#### **Gabriela Leal, 22 anos**

#### Nascida e criada em São Paulo.
#### Desde muito pequena ajudei a minha vó (japonesa) a cozinhar e peguei gosto para coisa. 

#### Minhas receitas vieram de muita tentativa e erro e espero que vocês gostem também. 

