---
title: "Udon"
date: 2022-10-08T00:07:42-03:00
draft: false
---
![Udon](http://www.saboresajinomoto.com.br/uploads/images/recipes/receita_udon.jpg)
#### Para dar aquele quentinho no coração em dias frios 

#### **Serve:** 4 pessoas
#### **Tempo de preparo:** 60 minutos

### **Ingredientes:**
#### - Uma cebola grande
#### - 2 dentes de alho
#### - Um punhado de alga do tipo konbu
#### - 4 shiitakes
#### - 1 pacote de kamaboko ou uzumaki
#### - 4 aburagues
#### - 1 xícara de shoyu
#### - 1/2 xícara de mirin
#### - 1/2 xícara de sake
#### - 500 mL de água
#### - 500g de macarrão tipo udon
#### - Cebolinha picada a gosto

> ----------

### **Modo de Preparo:**
#### 1. Pique a cebola e o alho e refogue em uma panela grande (para o caldo) até dourar. Adicione a água, o shoyu, o mirim e o sake.
#### 2. Pique o shiitake, o konbu, o aburague e o kamaboko e junte ao caldo. Deixe cozinhar por cerca de 30 min.
#### 3. Em outra panela, ferva a água e cozinhe o macarrão conforme as instruções do pacote. Escorra-o e reserve em uma vasilha. Dica! Separe em porções individuais para que seja mais fácil servir o macarrão. 
#### 4. Sirva em tigelas individuais, juntando o macarrão e o caldo apenas na hora de servir. 
#### 5. E o udon está pronto! Dica! Sirva junto com tempura ou karaague. 