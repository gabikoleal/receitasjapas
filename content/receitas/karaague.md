---
title: "Karaague"
date: 2022-10-08T00:07:42-03:00
draft: false
---
![Karaague](https://static01.nyt.com/images/2018/07/25/dining/HK-karaage-horizontal/merlin_141075300_74569dec-9fc2-4174-931d-019dddef3bb8-master768.jpg?w=1280&q=75)
#### Como negar um bom frango frito

#### **Serve:** 5 pessoas
#### **Tempo de preparo:** 60 minutos mais uma noite (marinada)

### **Ingredientes:**
#### - 1kg de filé de sobrecoxa de frango sem pele
#### - Uma cebola grande
#### - 4 dentes de alho
#### - Um pedaço de gengibre
#### - 1 xícara de Shoyu
#### - 1/2 xícara de mirin
#### - 1/2 xícara de sakê 
#### - Amido de milho
#### - Óleo para fritura
> ----------

### **Modo de Preparo:**
#### 1. Corte as sobrecoxas em peçados de mais ou menos 6 cm e reserve em uma vasilah grande.
#### 2. Pique a cebola e rale o alho e o gengibre bem fino. Adicione ao frango e misture bem.
#### 3. Despeje o shoyu, o mirin e o sake, bezuntando todos os pedaços de sobrecoxa. Cubra a vasilha e deixe o frago marinar por pelo menos uma noite. Caso os pedaços de frango não estejam submersos, vire-os na metade do tempo para que todos estejam bem marinados.
#### 4. Após o tempo de marinação, empane bem os pedaços com amido de milho. Dica! Caso queira uma crocancia extra, passe duas vezes no amido. 
#### 5. Em uma frigideira, esquente cerca de dois dedos de óleo a 180°C para fazer a fritura. Adicione os pedaços de frango e frite, em fogo baixo, por cerca de 5 minutos por lado, ou até que fique dourado. 
#### 6. Reserve os karaagues já prontos em um recipiente forrado com papel de pão ou papel toalha para que eles fiquem mais crocantes e sequinhos. 
#### 7. E os karaagues estão prontos! Sirva-os com shoyu ou molho tonkatsu.