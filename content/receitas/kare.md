---
title: "Kare"
date: 2022-10-08T00:07:42-03:00
draft: false
---
![Kare](https://i.ytimg.com/vi/bHtGO_Qmws0/maxresdefault.jpg)
#### Um clássico

#### **Serve:** 6 pessoas
#### **Tempo de preparo:** 90 minutos

### **Ingredientes:**
#### - 1kg de carne de segunda (músculo, acém)
#### - 2 cebolas grandes
#### - 4 dentes de alho
#### - 3 batatas grandes
#### - 1 cenoura média
#### - 1 maçã gala 
#### - 1 caixa de temperu curry japonês (Golden Curry) a seu gosto, nessa receita vamos fazer uma mistura metade amakuchi (suave picância) e metade chuukara (média picância).
> ----------

### **Modo de Preparo:**
#### 1. Pique e refogue uma cebola e o alho, em uma panela de pressão, até dourar, em seguida junte a carne e sele-a bem. 
#### 2. Cubra com água o suficiente e cozinhe em pressão por cerca de 30 minutos. 
#### 3. Enquanto a carne cozinha, descasque e pique em pedaços médios as batatas, a outra cebola , a cenoura e a maçã. Reserve
#### 4. Após o tempo de cozimento da carne, junte os vegetais cortados e cozinhe por mais 30 minutos. 
#### 5. Ao fim do cozimento, junte um tablete de tempero curry suave e um de média picância, misturando bem. 
#### 6. E o kare está prontos! Sirva com arroz e molho tonkatsu.