---
title: "Tempura"
date: 2022-10-08T00:07:34-03:00
draft: false
---

![Tempura](https://i.ytimg.com/vi/ZoSwZsUgNQM/maxresdefault.jpg)

#### O que todo mundo gosta

#### **Serve:** 4 pessoas
#### **Tempo de preparo:** 30 minutos

### **Ingredientes:**
#### - Uma cenoura média
#### - Uma cebola grande
#### - Um punhado de vagens
#### - Uma beringela grande
#### - Cebolinha a gosto
#### - Sal e pimenta do reino a gosto
#### - 2 colheres (sopa) de farinha de trigo
#### - 2 colheres (sopa) de água gelada
#### - 1 ovo
#### - Óleo para fritura

> ----------

### **Modo de Preparo:**
#### 1. Lave e corte os vegetais de forma que os pedaços sejam finos e compridos. Tempere com sal, pimenta e cebolinha. Reserve em uma tigela grande.
#### 2. Em outro recipiente, adicione a farinha, a água e o ovo e misture até ficar homogêneo.
#### 3. Despeje a massa nos vegetais e misture bem. Não é necessário que haja muita massa, mas todos os vegetais devem estar cobertos. 
#### 4. Em uma frigideira, esquente cerca de dois dedos de óleo a 180°C para fazer a fritura. Adicione porções do tempura e frite por cerca de 3 minutos por lado, ou até que fique dourado. 
#### 5. Reserve os tempuras já prontos em um recipiente forrado com papel de pão ou papel toalha para que eles fiquem mais crocantes e sequinhos. 
#### 6. E os tempuras estão prontos! Sirva-os com shoyu ou molho tonkatsu.




